FROM python:3.7
MAINTAINER Eugene Murawjow "e.muravev@innopolis.ru"
COPY ./requirements.txt /app/requirements.txt
WORKDIR app
RUN pip install -r requirements.txt
COPY . /app
CMD [ "python", "datanode.py" ]
